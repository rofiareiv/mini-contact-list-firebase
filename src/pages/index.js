import Home from './Home';
import AddContact from './AddContact';
import DetailContact from './DetailContact';
import EditContact from './EditContact';

export {Home, AddContact, DetailContact, EditContact};
