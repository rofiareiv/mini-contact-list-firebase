import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Home, AddContact, DetailContact, EditContact} from '../pages';

const Stack = createStackNavigator();

const Router = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="AddContact"
        component={AddContact}
        options={{title: 'Add Contact'}}
      />
      <Stack.Screen
        name="DetailContact"
        component={DetailContact}
        options={{title: 'Contact Detail'}}
      />
      <Stack.Screen
        name="EditContact"
        component={EditContact}
        options={{title: 'Edit Contact'}}
      />
    </Stack.Navigator>
  );
};

export default Router;
