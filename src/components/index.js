import InputGrup from './InputGrup';
import BtnGrup from './BtnGrup';
import Header from './Header';
import CardContact from './CardContact';
import BtnIcon from './BtnIcon';

export {InputGrup, BtnGrup, Header, CardContact, BtnIcon};
